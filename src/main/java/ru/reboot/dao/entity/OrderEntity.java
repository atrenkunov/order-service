package ru.reboot.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "orders")
public class OrderEntity {

    @Id
    @Column(name = "orderId")
    private String orderId;

}
