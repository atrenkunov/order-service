package ru.reboot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.reboot.dao.entity.OrderEntity;

public interface OrderRepository extends JpaRepository<OrderEntity, String> {
}
