package ru.reboot.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.reboot.dto.Order;
import ru.reboot.service.OrderService;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "orders")
public class OrderControllerImpl implements OrderController {

    private static final Logger logger = LogManager.getLogger(OrderControllerImpl.class);

    private OrderService orderService;

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("info")
    public String info() {
        logger.info("method .info invoked");
        return "OrderController " + new Date();
    }

    @Override
    @GetMapping("/all")
    public List<Order> getAllOrders() {
        return orderService.getAllOrders();
    }

    @Override
    @GetMapping("/order")
    public Order getOrderById(@RequestParam("orderId") String orderId) {
        return orderService.getOrderById(orderId);
    }

    @Override
    @GetMapping("/all/byPhoneNumber")
    public List<Order> getOrdersByPhoneNumber(@RequestParam("phoneNumber") String phoneNumber) {
        return orderService.getOrdersByPhoneNumber(phoneNumber);
    }

    @Override
    @PostMapping("/order")
    public Order createOrder(@RequestBody Order order) {
        return orderService.createOrder(order);
    }

    @Override
    @DeleteMapping("/order")
    public void deleteOrder(@RequestParam("orderId") String orderId) {
        orderService.deleteOrder(orderId);
    }


}
