package ru.reboot.error;

public enum ErrorCodes {
    ILLEGAL_ARGUMENT,
    ORDER_NOT_FOUND,
    CANT_CREATE_NEW_ORDER
}
