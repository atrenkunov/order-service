package ru.reboot.dto;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Order {

    private String orderId;
    private String phoneNumber;
    private String address;
    private LocalDateTime orderDate;
    private LocalDateTime deliveryDate;
    private Map<String, Integer> items;

    private Order() {
        items = new HashMap<>();
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public LocalDateTime getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDateTime deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Map<String, Integer> getItems() {
        return items;
    }

    public void setItems(Map<String, Integer> items) {
        this.items = items;
    }

    public void addItem(String itemId, int count) {
        this.items.put(itemId, count);
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId='" + orderId + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", orderDate=" + orderDate +
                ", deliveryDate=" + deliveryDate +
                ", items=" + items +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderId.equals(order.orderId) && phoneNumber.equals(order.phoneNumber) && address.equals(order.address) && orderDate.equals(order.orderDate) && deliveryDate.equals(order.deliveryDate) && items.equals(order.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, phoneNumber, address, orderDate, deliveryDate, items);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private final Order order;

        private Builder() {
            order = new Order();
        }

        public Builder orderId(String orderId) {
            order.orderId = orderId;
            return this;
        }

        public Builder phoneNumber(String phoneNumber) {
            order.phoneNumber = phoneNumber;
            return this;
        }

        public Builder address(String address) {
            order.address = address;
            return this;
        }

        public Builder orderDate(LocalDateTime orderDate) {
            order.orderDate = orderDate;
            return this;
        }

        public Builder deliveryDate(LocalDateTime deliveryDate) {
            order.deliveryDate = deliveryDate;
            return this;
        }

        public Builder addItem(String itemId, int count) {
            order.addItem(itemId, count);
            return this;
        }

        public Order build() {
            return order;
        }
    }
}
